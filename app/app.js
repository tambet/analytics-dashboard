(function() {
	var _ = {
		utils: {},
		views: {},
		models: {},
		services: {}
	};

	_.utils = {
		find: function(identifier) {
			return document.getElementById(identifier);
		},
		formatNumber: function(num) {
			return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		}
	};

	_.models.Chart = function(attributes) {
		var attrs = attributes || {};
		this.name = attrs.name || "";
		this.suffix = attrs.suffix || "";
		this.segments = attrs.segments || [];
		this.decorateSegments();
	};

	_.models.Chart.prototype = {
		get total() {
			return this.segments.reduce(function(sum, cur) {
				return sum + cur.value;
			}, 0);
		},
		get totalValue() {
			return _.utils.formatNumber(this.total) + this.suffix;
		},
		get identifier() {
			if (this._identifier) return this._identifier;
			return this._identifier = Math.random().toString(36).slice(2);
		},
		segmentValue: function() {
			return _.utils.formatNumber(this.value);
		},
		segmentPercentage: function() {
			return this.percentage.toFixed();
		},
		decorateSegments: function() {
			this.segments.forEach(function(segment) {
				segment.percentage = segment.value * 100 / this.total;
			}, this);
		}
	};

	_.services.Charts = function() {
		this.data = [
			{
				name: "Revenue",
				suffix: "€",
				segments: [
					{name: "Tablet", value: 120000, color: "#50d124" },
					{name: "Smartphone", value: 80000, color: "#065e00"}
				]
			},
			{
				name: "Impresions",
				segments: [
					{name: "Tablet", value: 20000000, color: "#09c5e3"},
					{name: "Smartphone", value: 30000000, color: "#00495b"}
				]
			},
			{
				name: "Visits",
				segments: [
					{name: "Tablet", value: 480000000, color: "#ffc000"},
					{name: "Smartphone", value: 120000000, color: "#d44e10"}
				]
			}
		];
	};

	_.services.Charts.prototype = {
		fetch: function () {
			return this.data.map(function(attrs) {
				return new _.models.Chart(attrs);
			});
		}
	};

	_.views.PieChart = function(options) {
		this.attrs = options.data;
		this.elem = _.utils.find(this.attrs.identifier);
	};

	_.views.PieChart.prototype = {
		get viewBox() {
			if (this._viewBox) return this._viewBox;
			var width = this.elem.offsetWidth;
			var height = this.elem.offsetHeight;
			return this._viewBox = Math.min(width, height);
		},
		get radius() {
			return (this.viewBox / 2).toFixed();
		},
		get outerRadius() {
			return this.radius - 5;
		},
		get segments() {
			return this.attrs.segments.slice().reverse();
		},
		get viewBoxString() {
			return "0 0 " + this.viewBox + " " + this.viewBox;
		},
		get transformString() {
			return "translate(" + this.radius + "," + this.radius + ")";
		},
		render: function() {
			var svg = d3.select(this.elem)
				.append("svg")
				.attr("width", "100%")
				.attr("height", "100%")
				.attr("viewBox", this.viewBoxString)
				.attr("preserveAspectRatio", "xMinYMin");

			var group = svg.append("g")
				.attr("transform", this.transformString);

			var arc = d3.svg.arc()
				.outerRadius(this.radius)
				.innerRadius(this.outerRadius);

			var pie = d3.layout.pie().sort(null)
				.value(function(d) { return d.value; });

			group.append("text")
				.attr("dy", "-.9em")
				.text(this.attrs.name)
				.attr("class", "pie-title");

			group.append("text")
				.attr("dy", ".5em")
				.text(this.attrs.totalValue)
				.attr("class", "pie-total");

			group.selectAll("arc")
				.data(pie(this.segments))
				.enter().append("g")
				.append("path")
				.attr("d", arc)
				.style("fill", function(d) { return d.data.color; });
		}
	};

	_.views.AppView = function() {
		this.charts = (new _.services.Charts).fetch();
		this.template = _.utils.find("charts-template").innerHTML;
	};

	_.views.AppView.prototype = {
		render: function() {
			this.html(Mustache.render(this.template, {charts: this.charts}));
			this.initializePieCharts();
		},
		html: function(value) {
			_.utils.find("app").innerHTML = value;
		},
		initializePieCharts: function() {
			this.charts.forEach(function(chart) {
				var pieChart = new _.views.PieChart({data: chart});
				pieChart.render();
			});
		}
	};

	self.app = _;
})();
