/*eslint-env jasmine */

describe("Model: Chart", function () {

	var model, app = self.app;

	beforeEach(function() {
		var attributes = {
			name: "Revenue",
			suffix: "€",
			segments: [
				{name: "Desktop", value: 10000, color: "#065e00"},
				{name: "Tablet", value: 120000, color: "#50d124" },
				{name: "Smartphone", value: 80000, color: "#065e00"}
			]
		};
		model = new app.models.Chart(attributes);
	});

	it("should have an identifier", function() {
		expect(model.identifier).not.toBe(null);
	});

	it("should have unique identifier", function() {
		var newModel = new app.models.Chart();
		expect(newModel.identifier).not.toEqual(model.identifier);
	});

	it("should calculate total of its segments", function() {
		expect(model.total).toEqual(210000);
	});

	it("should add percentage to segments", function() {
		expect(model.segments[0].percentage.toFixed()).toEqual("5");
		expect(model.segments[1].percentage.toFixed()).toEqual("57");
		expect(model.segments[2].percentage.toFixed()).toEqual("38");
	});

});
