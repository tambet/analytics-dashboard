# Getting Started

#### 1. Install gulp globally:

```sh
$ npm install --global gulp
```

#### 2. Install npm and bower dependencies:

```sh
$ npm install && bower install
```

#### 3. Start the app (with live-server):

```sh
$ gulp start
```

#### 4. Run code linting:

```sh
$ gulp lint
```

#### 4. Run test specs:

```sh
$ gulp spec
```
