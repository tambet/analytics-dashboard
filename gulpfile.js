var gulp = require('gulp');
var eslint = require('gulp-eslint');
var Server = require('karma').Server;
var liveServer = require("live-server");

gulp.task('lint', function() {
	return gulp.src(['app/*.js', 'test/spec/*.js'])
		.pipe(eslint())
		.pipe(eslint.format())
		.pipe(eslint.failAfterError());
});

gulp.task('spec', function (done) {
	new Server({
		singleRun: true,
		configFile: __dirname + '/test/karma.conf.js'
	}, done).start();
});

gulp.task('start', function() {
	liveServer.start({root: 'app'});
});

gulp.task('default', ['lint', 'spec'], function(){});
